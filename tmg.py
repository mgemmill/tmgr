'''
Time Manager

Usage:
    tm 
    tm start
    tm add entry [date] [time]
    tm add cat <category>
    tm sum [date]
    tm open
    tm get --entry=<last>

Options:
  -h --help     Show this screen.
  --version     Show version.

'''

# [timestamp]|[category]|[incident]|[notes]|[duration]
import os
from datetime import datetime
from collections import namedtuple


TMSMP_FORMAT = '%Y-%m-%d %H:%M:%S'

class Entry(namedtuple('Entry', 'timestamp category incident notes duration')):

     __slots__ = ()

     def __str__(self):
         return '''Category:  {1}\nIncident:  {2}\nNotes:     {3}\ntimestamp: {0}\nduration:  {4}\n'''.format(*self)


def tail(file_path, window=1):
    with open(file_path, 'r') as f:
        BUFSIZ = 1024
        f.seek(0, 2)
        bytes = f.tell()
        size = window
        block = -1
        data = []
        while size > 0 and bytes > 0:
            if (bytes - BUFSIZ > 0):
                # Seek back one whole BUFSIZ
                f.seek(block*BUFSIZ, 2)
                # read BUFFER
                data.append(f.read(BUFSIZ))
            else:
                # file too small, start from begining
                f.seek(0,0)
                # only read what was not read
                data.append(f.read(bytes))
            linesFound = data[-1].count('\n')
            size -= linesFound
            bytes -= BUFSIZ
            block -= 1
        return ''.join(data).splitlines()[-window:]


def get_appdir():
    appdir = os.environ.get('APPDATA')
    appdir = os.path.join(appdir, 'tmgr')
    if not os.path.exists(appdir):
        os.makedirs(appdir)
    return appdir


def config():
    class Config(object): pass
    c = Config()
    c.appdir = appdir = get_appdir()
    c.dbfile = os.path.join(appdir, 'entries.db')
    c.catfile = os.path.join(appdir, 'categories.db')
    return c


def record_entry(db_file, category, incident, notes, duration=None, timestamp=None):

    duration = '' if not duration else str(int(duration))
    timestamp = format_time(parse_time(timestamp))

    with open(db_file, 'a') as fh_:
        record = [timestamp, category, incident, notes, duration]
        fh_.write('|'.join(record))
        fh_.write('\n')
        

def read_entry(entry):
    timestamp, category, incident, notes, duration = entry.split('|')
    timestamp = datetime.strptime(timestamp, TMSMP_FORMAT)
    return Entry(timestamp, category, incident, notes, duration)


def record_category(conf, category):
    categories = read_categories(conf.catfile)
    if category.lower() in categories:
        return
    with open(conf.catfile, 'a') as fh_:
        fh_.write(category.lower())
        fh_.write('\n')


def read_categories(dbfile):
    if not os.path.exists(dbfile):
        return []
    with open(dbfile, 'r') as fh_:
        return [line.strip() for line in fh_.readlines()]


def fetch_last_entry(conf):
    return read_entry(tail(conf.dbfile)[0])
    
def fetch_entries(conf, filter):
    with open(conf.dbfile, 'r') as fh_:
        return [read_entry(entry) for entry in fh_.readlines() if filter(entry)]


def display_summary(conf, date_str):
    if not date_str:
        date_str = datetime.now().strftime('%Y-%m-%d')
    def make_filter():
        def _filter(entry_str):
            return entry_str.startswith(date_str)
        return _filter
    entries = fetch_entries(conf, make_filter())
    for e in entries:
        print str(e)

    total_hours = sum([int(e.duration) for e in entries if e.category != 'start']) / 60.0
    break_hours = sum([int(e.duration) for e in entries if (e.category != 'start' and e.category == 'break')]) / 60.0
    edi_hours = sum([int(e.duration) for e in entries if (e.category != 'start' and e.category.startswith('edi'))]) / 60.0
    site_hours = sum([int(e.duration) for e in entries if (e.category != 'start' and e.category.startswith('site'))]) / 60.0
    other_hours = sum([int(e.duration) for e in entries if (e.category not in ('start', 'break') and not e.category.startswith('site') and not e.category.startswith('edi'))]) / 60.0

    print '------------------'
    print 'total hours: {0}'.format(total_hours)
    print 'break hours: {0}'.format(break_hours)
    print 'work hours:  {0}'.format(total_hours - break_hours)
    print '------------------'
    print 'edi hours:   {0}'.format(edi_hours)
    print 'site hours:  {0}'.format(site_hours)
    print 'other hours: {0}'.format(other_hours)
    print '------------------'




def display_entry(conf, selector):
    entry = fetch_last_entry(conf)
    print '\n{0}\n'.format(entry)


def parse_time(timestamp):
    if not timestamp:
        return datetime.now()
    if isinstance(timestamp, datetime):
        return timestamp
    elif isinstance(timestamp, basestring):
        return datetime.strptime(timestamp, TMSMP_FORMAT)
    else:
        raise Exception('Invalid time: {0}'.format(timestamp))

def format_time(timestamp):
    if isinstance(timestamp, datetime):
        return timestamp.strftime(TMSMP_FORMAT) 
    elif isinstance(timestamp, basestring):
        # assume its a timestamp string
        return timestamp
    else:
        raise Exception('Invalid time: {0}'.format(timestamp))


def calc_duration(end_time, start_time):
    # return minutes
    start_time = parse_time(start_time)
    end_time = parse_time(end_time)
    duration = end_time - start_time
    return duration.seconds // 60


def time_entry(conf):
    last_entry = fetch_last_entry(conf)

    category = raw_input('Category: ').lower()
    incident = raw_input('Incident: ')
    notes =    raw_input('Notes:    ')

    date_str =    raw_input('Date:     ')
    if not date_str:
        # not response leads to automatic
        date_ = datetime.now()
    else:
        date_ = parse_time(date_str)
    # set the duration
    duration = str(calc_duration(date_, last_entry.timestamp))
    duration_str = None

    if date_str:
        # if we've input a datetime, then we just want the calculated duration
        print 'Duration: {0}'.format(duration)
    else:
        duration_str = raw_input('Duration: ')

    if duration_str:
        # we are setting the duration manually
        duration = last_entry + datetime.timedelta(0, int(duration_str))

    record_entry(conf.dbfile, category, incident, notes, timestamp=date_, duration=duration)


def start_entry(conf):
    record_entry(conf.dbfile, 'start', 'start', 'start')


def open_entry_file(conf):
    import subprocess
    subprocess.call([r'C:\Program Files (x86)\Vim\vim74\gvim.exe', conf.dbfile])


def main():
    import docopt
    args = docopt.docopt(__doc__, version='Time Manager 0.1')
    conf = config()
    print ''
    if args['start']:
        start_entry(conf)
    elif args['add'] and args['cat']:
        record_category(conf, args['<category>'])
    elif args['sum']:
        display_summary(conf, args['date'])
    elif args['get']:
        display_entry(conf, args['--entry'])
    elif args['open']:
        open_entry_file(conf)
    else:
        time_entry(conf)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt as ex:
        print '\n\nprocess canceled'
    raw_input('\n..')
